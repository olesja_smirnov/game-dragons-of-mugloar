package com.example.dragons.service;

import java.util.List;

import com.example.dragons.model.*;

public interface RestService {

    Game startGame();

    Reputation investigateReputation(String gameId);

    List<MessagesTasks> getAllMessages(String gameId);

    List<Shop> getItemsFromShop(String gameId);

    Purchase purchaseItemFromShop(String gameId, String itemId);

    Message solveMessage(String gameId, String adId);

}