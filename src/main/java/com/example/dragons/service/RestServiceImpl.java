package com.example.dragons.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import com.example.dragons.model.*;

@Service
public class RestServiceImpl implements RestService {

    @Value("${value.dragons.gameUrl}")
    String gameUrl;

    private RestTemplate restTemplate;

    public RestServiceImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Game startGame() {
        return this.restTemplate.postForObject(gameUrl + "game/start", null, Game.class);
    }

    @Override
    public Reputation investigateReputation(String gameId) {
        String url = String.format(gameUrl + gameId + "/investigate/reputation");
        return this.restTemplate.postForObject(url, null, Reputation.class);
    }

    @Override
    public List<MessagesTasks> getAllMessages(String gameId) {
        String url = String.format(gameUrl + gameId + "/messages");
        ResponseEntity<List<MessagesTasks>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<MessagesTasks>>(){});
        return response.getBody();
    }

    @Override
    public Message solveMessage(String gameId, String adId) {
        String url = String.format(gameUrl + gameId + "/solve/" + adId);
        return this.restTemplate.postForObject(url, null, Message.class);
    }

    @Override
    public List<Shop> getItemsFromShop(String gameId) {
//        return this.restTemplate.getForObject(gameUrl + gameId + "/shop", Shop[].class);
        String url = String.format(gameUrl + gameId + "/shop");
        ResponseEntity<List<Shop>> response = restTemplate.exchange(
                url,
                HttpMethod.GET,
                null,
                new ParameterizedTypeReference<List<Shop>>(){});
        return response.getBody();
    }

    @Override
    public Purchase purchaseItemFromShop(String gameId, String itemId) {
        String url = String.format(gameUrl + gameId + "/shop/buy/" + itemId);
        return this.restTemplate.postForObject(url, null, Purchase.class);
    }

}