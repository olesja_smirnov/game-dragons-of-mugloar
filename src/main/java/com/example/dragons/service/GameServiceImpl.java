package com.example.dragons.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.dragons.model.*;

@Service
public class GameServiceImpl implements GameService {

    private Game game;

    private Logger loggerFactory = LoggerFactory.getLogger(this.getClass().getPackage().getName());

    @Autowired
    private RestService restService;

    public GameServiceImpl(RestService restService) {
        this.restService = restService;
    }

    @Override
    public void beginGame() {

        game = restService.startGame();
        while(game.gameLives()) {

            if(game.getGold() > 100) {
                purchaseItem();
            }

            Reputation reputation = restService.investigateReputation(game.getGameId());
            List<MessagesTasks> allMessages = restService.getAllMessages(game.getGameId());
            MessagesTasks oneMessage = allMessages.get(0);

            loggerFactory.info("LOG: " + oneMessage.getMessage() + " and get reward " + oneMessage.getReward() + ". " + oneMessage.getProbability());

            Message solvedMessage = restService.solveMessage(game.getGameId(), oneMessage.getAdId());
            loggerFactory.info("LOG: "+ solvedMessage.getMessage() + " Your have " + game.getLives() + " lives"
                    + ", " + game.getGold() + " gold and your score is " + game.getScore());
            updateGameData(solvedMessage);

        }
        loggerFactory.info("LOG: YOUR FINAL SCORE IS " + game.getScore() + " and gold is " + game.getGold() + ". Highscore is " + game.getHighScore());
    }

    private void updateGameData(Message message) {
        game.setLives(message.getLives());
        game.setGold(message.getGold());
        game.setScore(message.getScore());
    }

    private void purchaseItem() {

        List<Shop> availableItems = restService.getItemsFromShop(game.getGameId())
                .stream()
                .filter(item -> (game.getGold() >= item.getCost()))
                .collect(Collectors.toList());

        if(availableItems.size() > 0 ) {
            List<String> purchasedItems = new ArrayList<>();
            Random random = new Random();
            Shop randomItemFromShop = availableItems.get(random.nextInt(availableItems.size()));
            Purchase purchase = restService.purchaseItemFromShop(game.getGameId(), randomItemFromShop.getId());
            game.setGold(purchase.getGold());
            loggerFactory.info("LOG: You have purchased " + randomItemFromShop.getName()
                    + " with the cost of " + randomItemFromShop.getCost() + " of gold.");
            purchasedItems.add(randomItemFromShop.getId());
        }
    }

}