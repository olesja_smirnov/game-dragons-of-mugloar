package com.example.dragons.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Message {

    private Boolean success;
    private Integer lives;
    private Integer gold;
    private Integer score;
    private Integer highScore;
    private Integer turn;
    private String message;

    public Message(Boolean success, Integer lives, Integer gold, Integer score, Integer highScore, Integer turn, String message) {
        this.success = success;
        this.lives = lives;
        this.gold = gold;
        this.score = score;
        this.highScore = highScore;
        this.turn = turn;
        this.message = message;
    }

}