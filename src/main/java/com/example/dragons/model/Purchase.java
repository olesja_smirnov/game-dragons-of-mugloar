package com.example.dragons.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Purchase {

    private String shoppingSuccess;
    private Integer gold;
    private Integer lives;
    private Integer level;
    private Integer turn;

    public Purchase(String shoppingSuccess, Integer gold, Integer lives, Integer level, Integer turn) {
        this.shoppingSuccess = shoppingSuccess;
        this.gold = gold;
        this.lives = lives;
        this.level = level;
        this.turn = turn;
    }

}