package com.example.dragons.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Reputation {

    public Integer people;
    public Integer state;
    public Integer underworld;

    public Reputation(Integer people, Integer state, Integer underworld) {
        this.people = people;
        this.state = state;
        this.underworld = underworld;
    }

}