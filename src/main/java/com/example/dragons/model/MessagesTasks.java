package com.example.dragons.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class MessagesTasks {

    private String adId;
    private String message;
    private String reward;
    private Integer expiresIn;
    private String probability;

    public MessagesTasks(String adId, String message, String reward, Integer expiresIn, String probability) {
        this.adId = adId;
        this.message = message;
        this.reward = reward;
        this.expiresIn = expiresIn;
        this.probability = probability;
    }

}