package com.example.dragons.model;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Shop {

    private String id;
    private String name;
    private Integer cost;

    public Shop(String id, String name, Integer cost) {
        this.id = id;
        this.name = name;
        this.cost = cost;
    }

}