package com.example.dragons.model;

import java.io.Serializable;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Game implements Serializable {

    private String gameId;
    public Integer lives;
    public Integer gold;
    public Integer level;
    public Integer score;
    public Integer highScore;
    public Integer turn;

    public Game(String gameId, Integer lives, Integer gold, Integer level, Integer score, Integer highScore, Integer turn) {
        this.gameId = gameId;
        this.lives = lives;
        this.gold = gold;
        this.level = level;
        this.score = score;
        this.highScore = highScore;
        this.turn = turn;
    }

    public Boolean gameLives() {
        return lives > 0;
    }

}