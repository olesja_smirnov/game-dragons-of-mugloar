package com.example.dragons;

import springfox.documentation.swagger2.annotations.EnableSwagger2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import com.example.dragons.service.GameService;

@SpringBootApplication
public class DragonsOfMugloarApplication implements CommandLineRunner {

    private Logger loggerFactory = LoggerFactory.getLogger(this.getClass().getPackage().getName());

    @Autowired
    private ApplicationContext context;

    @Autowired
    private GameService game;

    public static void main(String[] args) {
        SpringApplication.run(DragonsOfMugloarApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

            loggerFactory.info("LOG: Starting new game....");
            game.beginGame();

            SpringApplication.exit(context, () -> 0);
            loggerFactory.info("LOG: Game is over.");
    }

}